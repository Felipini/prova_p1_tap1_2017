package controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import helper.JsonHelper;
import model.Automovel;

@WebServlet(urlPatterns = "autocontroller")
public class AutoController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private List<Object> lista = new ArrayList<>();
	
	private JsonHelper jsonHelper =  new JsonHelper();
			
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nome = req.getParameter("nome");
		String marca = req.getParameter("marca");
		String placa = req.getParameter("placa");
		double valor = Double.parseDouble(req.getParameter("valor"));

		
		Automovel auto = new Automovel(nome, marca, placa, lista.size()+1, valor);
		lista.add(auto);
		

		
		
		try {
			resp.getWriter().println(jsonHelper.gerarJson(auto));
	
		
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	
	
	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		String json;
		
		try {
			json = jsonHelper.gerarJsonLista(lista);
			resp.getWriter().print(json);
		} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			String placa = req.getParameter("placa");
			for(int i=0;i<lista.size();i++){
				if(placa == ((Automovel) lista.get(i)).getPlaca()){

						try {
							json = jsonHelper.gerarJson(lista.get(i));
							resp.getWriter().print("O carro: "+json);
						} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							}
				}
			}
			
			int qtd = 0;
			for(int i=0;i<lista.size();i++){
				qtd = qtd + 1;
			}
			resp.getWriter().print("Quantidade de carros: "+qtd);
					
			
	}

	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nome = req.getParameter("nome");
		String marca = req.getParameter("marca");
		String placa = req.getParameter("placa");
		double valor = Double.parseDouble(req.getParameter("valor"));
		int id = Integer.parseInt(req.getParameter("id"));
		
		for(int i=0;i<lista.size();i++){
			if(id == ((Automovel) lista.get(i)).getId()){
				((Automovel) lista.get(i)).setNome(nome);
				((Automovel) lista.get(i)).setMarca(marca);
				((Automovel) lista.get(i)).setPlaca(placa);
				((Automovel) lista.get(i)).setValor(valor);
			}
		}
	}

	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id = Integer.parseInt(req.getParameter("id"));
		for(int i=0;i<lista.size();i++){
			if(id == ((Automovel) lista.get(i)).getId()){
				lista.remove(i);
			}

		}
	}
}