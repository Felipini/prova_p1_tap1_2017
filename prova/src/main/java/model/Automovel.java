package model;

public class Automovel {
	
	private String marca, nome, placa;
	private double valor;
	public int id;
	
	public Automovel(String marca, String nome, String placa, int id, double valor){
		this.nome = nome;
		this.marca = marca;
		this.id = id;
		this.placa = placa;
		this.valor = valor;		
	}
	
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getMarca() {
		return marca;
	}
	public String getNome() {
		return nome;
	}
	public String getPlaca() {
		return placa;
	}
	public int getId() {
		return id;
	}
	public double getValor() {
		return valor;
	}
	
	

}
